const grid = document.querySelector('.grid');
let gridValue = document.querySelector('.grid-size');
let gridSize = document.querySelector('input');
const resetBtn = document.querySelector('.reset');
const applyBtn = document.querySelector('.apply');
let squareSize = 8;

createGrid(squareSize);

// Creating one squared div pixel
function createDiv(size) {
    const div = document.createElement('div');
    div.classList.add('box');
    div.style.width = `${size}px`;
    div.style.height = `${size}px`;
    return div;
}

// Creating the grid with multiple div pixels
function createGrid(gridSize) {
    for (let i = 0; i < gridSize; i++) {
        for (let j = 0; j < gridSize; j++) {
            grid.appendChild(createDiv(grid.clientWidth / gridSize));
        }
    }
}

// Reseting the grid
function reset() {
    while (grid.firstChild) {
        grid.removeChild(grid.lastChild);
    }
    createGrid(squareSize);
}

//Painting
grid.addEventListener('mouseover', function (e) {
    // Add the "active" class to only divs with a "box" class
    if (e.target.matches('.box')) {
      e.target.classList.add('active');
    }
  });

// Getting input grid size
gridSize.addEventListener('input', function (e) {
    squareSize = e.target.value;
    gridValue.textContent = `${squareSize}x${squareSize}`;
});

//
applyBtn.addEventListener('click', function () {
    reset();
  });
  
resetBtn.addEventListener('click', reset);